title: Contribuer

Le site est conçu pour favoriser les contributions, ajouts et remarques. Le dépôt principal est hébergé sur gitlab. Un dépôt secondaire sur github peut également recevoir des contributions.

## Réagir, commenter
Il est possible d'adresser [un mail au collectif][mail] en indiquant clairement l'adresse de la page concernée.

La méthode privilégiée reste de [déposer un ticket sur le dépôt][issues].

## Proposer un contenu
Les méthodes déjà évoquées restent valides pour proposer un contenu, un retour d'expérience ou une référence nouvelle.

Le format préféré est la [syntaxe markdown](./aide/syntaxe_markdown.md), car il facilite le travail d'intégration, mais n'est pas obligatoire. En fonction du type de contribution certains éléments sont attendus.

??? abstract "Pour les outils"
    * une _partie introductive_, présentant les **enjeux** et intérêts de l'outils
    * une partie **mise en oeuvre**, indiquant **étapes à suivre** ou démarches à mener
    * une partie **recommandations**, plutôt centrée sur les **bonnes pratiques** et les **écueils** à éviter
    * si possible une partie **outils juridiques et financiers mobilisables** pour faciliter l'appropriation par acteurs
    * éventuellement une partie _références_ pour renvoyer vers des fiches plus complètes ou des exemples

??? abstract "Pour les études et guides"
    * **les auteurs** et **l'éditeur** qu'il s'agisse de personnes physiques ou morales
    * **la date** d'édition
    * si possible une **adresse de téléchargement** ou sur le site de l'éditeur

??? abstract "Pour les retours d'expérience"
    * **les contacts** à privilégier pour en savoir plus
    * si possible **un site internet** proposant des informations complémentaires

## Directement au code
Il est également possible de [soumettre des modifications plus directement](./aide/contribuer_au_depot.md) aux pages via le code du site. Les propositions se font sur la branche _contributions_, indifféremment sur Gitlab ou Github.

Il suffit de demander un accès sur l'une de ces plate formes. Un compte est nécessaire. Pour les moins ferrus de technique, nous recommandons l'utilisation de stackedit.io

[mail]: mailto:incoming+foncier-eco-foncier-eco-gitlab-io-14636881-issue-@incoming.gitlab.com?subject=Foncier-Eco
[issues]: https://gitlab.com/foncier-eco/foncier-eco.gitlab.io/issues
