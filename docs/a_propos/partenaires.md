title: Les partenaires du Collectif Foncier

## Les "permanents"

_Pour l'heure il n'y a pas de secrétariat désigné au_ Collectif. _Vous pouvez toutefois utiliser le système de tickets pour adresser vos remarques et commentaires. Des liens figurent au bas de chaque page du site_.

<div markdown="1" class="three cols">
!!! contact "CCI Normandie"
    Laurent Lesimple<br/>
    <i class="m-icons petit">public</i> [site internet](https://www.normandie.cci.fr/)


!!! contact "Département de l'Eure"
    Anne-Laure Urbain<br/>
    <i class="m-icons petit">public</i> [site internet](https://eureennormandie.fr/accueil/les-actions-du-departement/economie-numerique/)


!!! contact "Région Normandie"
    Virginie Grenet, Fabrice Fossey<br/>
    <i class="m-icons petit">public</i> [site internet](https://normandie.fr)


!!! contact "Chambre d'Agriculture"
    Mathieu Dewulf<br/>
    <i class="m-icons petit">public</i> [site internet](https://normandie.chambreagri.fr)


!!! contact "Chambre des métiers"
    Bernard Behot<br/>
    <i class="m-icons petit">public</i> [site internet](https://cm-27.fr/)     

!!! contact "EPF Normandie"
    Franck Fourreau<br/>
    <i class="m-icons petit">public</i> [site internet](https://epf-normandie.fr)


!!! contact "DDTM et DREAL de Normandie"
    Caroline Maury, Hélène Buhot, Hervé Lericolais<br/>
    <i class="m-icons petit">public</i> [site internet](http://normandie.developpement-durable.gouv.fr)<br/>
    _Sylvain Comte_

!!! contact "CEREMA"
    Gaëlle Schauner<br/>
    <i class="m-icons petit">public</i> [site internet](https://www.cerema.fr/fr/cerema/directions/cerema-normandie-centre)<br/>
    _Nathalie Léglise_

</div>

## Ils contribuent (ou ont contribué) au Collectif

<div markdown="1" class="quatre colonnes">
* [PRAXIDEV](http://praxidev.com/)
* [Dixit](https://dixit.net/)
* [FIDAL](https://www.fidal.com/fr/region/normandie)
</div>
