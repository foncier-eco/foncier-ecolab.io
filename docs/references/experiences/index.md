title: Retours d'expériences

Dans un sujet aussi complexe que celui de l'optimisation du foncier, une approche pratique est souvent essentielle. Elle permet de mieux appréhender une logique de système souvent difficile à comprendre de manière purement théorique.

Cette section du site vise à rassembler une sélection de retours d'expériences et ne vise pas l'exhaustivité. Pour une vision plus large, n'hésitez pas à visiter les sites suivants qui rassemblent plus largement les initiatives menées.

<div markdown="1" class="three cols">
!!! abstract "Optigede"
    Destinée aux collectivités et aux entreprises, [OPTIGEDE](https://www.optigede.ademe.fr/) est une plateforme d'échanges et de diffusion d'outils et retours d'expérience sur l'économie circulaire et les déchets hébergée par l'ADEME.

!!! abstract "NECI"
    [Normandie Economie Circulaire](https://neci.normandie.fr)  vise à identifier les acteurs, les ressources, les initiatives et à favoriser leur mise en réseau afin de créer un écosystème régional de l’économie circulaire.

</div>
