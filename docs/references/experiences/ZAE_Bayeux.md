title: Etude pour la requalification de la Zone d'activités de la résistance de Bayeux

[L'EPF de Normandie][EPFN] a lancé sur le territoire de Bayeux Intercom une étude cofinancée par la [Région Normandie][Region_Normandie] visant à proposer des actions en vue de la requalification de cette zone d'activité vieillissante mais proche de la gare et du centre ville.

Accompagné par le bureau d'étude EAI, les propositions d'amélioration portent sur les espaces publics et des préconisations d'interventions sur les emprises privées pour une densification ou une optimisation des emprises bâties.

_Prochainement une synthèse de l'étude et des limites de l'exercice_
