authors: CCI CA DREAL Région SAFER et EPF Normandie, DDTM de Seine-Maritime, PNR Boucles de la Seine, Agences d'urbanisme de Rouen Le Havre et Caen, CEREMA
editor: CCI Normandie
title: Evaluer le potentiel foncier dans le cadre du PLUi
date: juin 2018
summary: mémo technique à l’usage des collectivités normandes pour assurer une plus grande cohésion dans la gestion du foncier à l’échelle régionale. Ce guide est le fruit d’un partenariat, composé d’organismes publics.
tags: urbanisme guide densification
url: https://www.normandie.cci.fr/ensemble-pour-une-gestion-plus-coherente-des-ressources-foncieres
img: evaluer_potentiel_foncier-Norm-2018.jpg

![couverture >](evaluer_potentiel_foncier-Norm-2018.jpg) Ce _mémo technique_ fournit des pistes aux collectivités en charge de l'élaboration des documents d'urbanisme pour l'identification des potentiels fonciers sur leur territoire.

Issu des travaux du [collectif foncier](../../a_propos/partenaires.md) normand, sous la coordination de la Chambre de Commerce et d’Industrie Normandie, le document propose un langage commun aussi bien en termes de définitions que de méthodes pour assurer une cohérence autour de la question sensible du foncier. Son élaboration a également conduit à  s’interroger sur les thématiques de la revalorisation des centre villes et cœurs de bourgs, de la diversification de la production foncière et de la requalification des zones d’activités économiques.

Il présente une méthode didactique en 4 étapes afin d’évaluer le potentiel foncier du territoire à la fois pour l'habitat et pour l'activité économique. Cette publication fait suite à un premier numéro publié en 2015, qui était consacré à la mesure de la consommation d’espace.
