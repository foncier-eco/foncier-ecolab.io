title: Objectif « Zéro artificialisation nette » : quels leviers pour protéger les sols?
authors: Julien Fosse, Julia Belaunde, Marie Dégremont, Alice Grémillet
editor: France Stratégie
date: juillet 2019
summary: ce rapport public fait le point sur les notions, mécanismes et acteurs à l'oeuvre autour de l'artificialisation des sols et propose des pistes d'action ainsi qu'un agenda
tags: guide artificialisation
url: https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000639.pdf
img: rapport-artificialisation-fs-2019.png

![couverture >](rapport-artificialisation-fs-2019.jpg) Ce _rapport public_ rappelle un certain nombre de notions essentielles autour de l'artificialisation, sa définition et ses modalités de mesure.

Mettant ensuite en avant quelques déterminants essentiels de la consommation d'espaces il propose des pistes d'action ou d'évolution des politiques publiques ainsi qu'un agenda de mise en oeuvre.
