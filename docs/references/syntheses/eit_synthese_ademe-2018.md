title: L'EIT, un outil au service des territoires
authors: ADEME
date: décembre 2018
tags: EIT déchets expériences
summary: Synthèse thématique autour d'expériences menées sur la question des déchets dans les projets d'écologie industrielle et territoriale
url: https://www.ademe.fr/ecologie-industrielle-territorriale-outil-service-territoires-l

Depuis 2009, des collectivités soutenues par l'ADEME se sont engagées dans des plans et programmes de prévention des déchets, puis dans des programmes "Territoire zéro déchet zéro gaspillage".

Elles partagent leurs retours d'expérience au sein du réseau A3P. Les animateurs de ces programmes contribuent activement à la rédaction des fiches "action-résultat", consultables sur le site [OPTIGEDE](https://www.optigede.ademe.fr/), permettant ainsi de capitaliser l'expérience de ces territoires.

Cette synthèse thématique permet un accès facilité à un panel (non exhaustif) de retours d'expérience en proposant :

* une description des approches mise en place par les collectivités,
* la présentation d'une sélection de fiches rendant compte de la diversité des actions menées sur les territoires,
* des ressources complémentaires.
