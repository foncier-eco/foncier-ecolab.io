title: Scénarios illustratifs

La multiplicité des déterminants, la situation unique de chaque espace d’activité (situation géographique, dimensions, type d’activité, environnement économique ou urbain…) rendent très complexe la modélisation des phénomènes à l’œuvre, d’autant qu’ils sont souvent en interaction les uns avec les autres.

Cependant, la mise en évidence de méthodes et d’outils visant à optimiser l’usage du foncier est presque impossible sans le support d’une situation, même virtuelle, qui permette à une collectivité ou à une entreprise de transposer le travail sur un contexte bien réel. Nous avons donc imaginé à partir de ce qui constitue le cycle de vie d’une ZAE, des situations courantes, extrapolées d’exemples rencontrés et qui permettront de « projeter » la mise en œuvre de méthodes ou d’outils.

Les cas d’usages, que nous avons baptisé « scénarios », ont été définis en retenant quelques principes importants :

* Il s’agit d’une contextualisation d’une situation réaliste à plusieurs échelles (région, intercommunalité, ZAE, parcelle etc.),
* Elle permet l’identification et la description d’acteurs de l’aménagement courants (intercommunalité, aménageur public, entreprise etc.),
* Le scénario met en valeur une intention ou un objectif : ex. création d’une ZAC, rachat d’un foncier non bâti par une collectivité, location ou occupation temporaire d’un terrain de réserve foncière d’une entreprise…

Six « scénarios » couvrant le spectre du **cycle de vie d’une ZAE** (_illustration_) ont ainsi été identifiés :

<div markdown="1" alt="Le cycle de vie des ZAE >">
![cycle de vie d'une ZAE](schema_cycle_foncier.svg)

</div>

* La requalification d'une ZAE :
    * Scénario A – Redynamiser un tissu économique par la requalification d’une ZAE
* La vie de la ZAE :
    * Scénario B – Assurer gestion et animation d’une ZAE sur le long terme
* La phase de commercialisation d’une ZAE :
    * Scénario C - Optimiser l’usage foncier au moment de la commercialisation
* Les étapes de programmation et de conception :
    * Scénario D - Aménager une nouvelle ZAE en secteur non métropolitain
    * Scénario E - Développer une ZAE métropolitaine en secteur stratégique
    * Scénario F - Intégrer une offre locative mutualisée au sein d’une ZAE
