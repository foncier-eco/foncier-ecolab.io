title: Fiches actions

Certaines des fiches outils et méthodes qui figurent sur ce site ont initialement été élaborées dans le cadre de l'étude et sont recensées ici pour mémoire. Elles ont pu évoluer depuis l'édition de l'étude.

* [Observatoire du foncier économique](../../../outils/connaissance/observatoire_foncier_eco.md)
* [Stratégies territoriales de développement économique](../../../outils/strategie_territoriale/index.md)
* [Mettre en place une gestion de zone d’activités](../../../outils/vie_ZA/gestion_ZA.md)
* [Animer une zone d’activités](../../../outils/vie_ZA/gestionnaire_animateur.md)
* [Installer une démarche EIT](../../../outils/EIT/installer_demarche_eit.md)
    * [Exemples de mutualisations](../../../outils/EIT/exemples_mutualisations.md)
    * [Exemples de synergies possibles](../../../outils/EIT/exemples_synergies.md)
* [La gestion collective de l’environnement](../../../outils/EIT/gestion_collective_environnement.md)
* [Optimisation de la performance du foncier et du bâti](../../../outils/foncier/optimisation_performance_foncier_et_bati.md)
