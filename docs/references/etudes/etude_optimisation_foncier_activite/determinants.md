title: Les déterminants de la sous-densité

Les causes de la sous-optimisation de l’usage du sol des Zones d'Activités Economiques sont multiples. Le travail de recherche et de dialogue engagé pour cette étude a permis d’en identifier un certain nombre, aux impacts différenciés selon les territoires.

## Documents stratégiques et planification
Très en amont de la réalisation d’une ZAE, les stratégies développées par les territoires à des échelles larges et prospectives, ainsi que leurs déclinaisons réglementaires dans les documents de planification ont un impact important sur l’efficacité de la consommation des sols. Plusieurs difficultés apparaissent :

Les documents stratégiques traitent en général la question du développement économique par l’offre foncière, mais sans réelle **vision stratégique et prospective** :

* Les besoins réels de foncier économique sont insuffisamment objectivés pendant l’élaboration des SCoT et PLU(i) ;
* L’approche prospective, étayée sur des méthodes d’évaluation fiables, dépassant la seule extrapolation de chiffres de la consommation foncière passée, est en général absente ;
* Les collectivités disposent de peu de moyens pour développer ces sujets dans leurs cahiers des charges. L’économie de moyen des études se fait notamment sur ce poste ;
* Finalement, le dimensionnement des besoins est souvent surévalué et la localisation des ZAE est parfois non pertinente.

[Les SCoT](../../../outils/planification/SCoT.md) sont en général peu efficaces en termes de développement économique :

* Les périmètres des SCoT ne reflètent souvent pas les échelles pertinentes du développement économique vécu par les entreprises qui se positionnent par rapport aux flux, plutôt que par rapport aux limites administratives ;
* L’analyse des besoins fonciers à vocation économique est souvent faussée par la priorité donnée dans les études des SCOT à une logique spatiale et de compromis entre les acteurs de l’aménagement concernés ;
* Les réflexions interSCoT restent rares alors qu’elles permettraient une meilleure coordination stratégique et la mise en valeur des atouts des territoires – en particulier en termes de multimodalité ;

Une prise en compte peu pertinente des enjeux du **foncier à vocation économique** dans [les PLU(i)](../../../outils/planification/PLUi.md) :

* La localisation géographique des ZAE est souvent opportuniste avec un déficit de stratégie d’implantation des entreprises entre tissu urbain, zone dense, périphérie, etc. en fonction de leurs besoins réels et de l’intérêt du territoire concerné ;
* Les potentiels de renouvellement urbain sont rarement identifiés précisément et leur mobilisation paraît souvent complexe, au point d’être négligée ;
* Les acteurs de l’aménagement anticipent des difficultés de mise en œuvre des projets de ZAE (conflits avec des tiers, difficultés pour acquérir les terrains, allongement des délais d’aménagement à la suite des fouilles archéologiques préventives, etc.) avec un surdimensionnement des réserves foncières.

!!! idee "Quelques pistes pour améliorer la situation"
    L’optimisation passe par une stratégie à l’échelle d’un territoire de projet de développement économique, souvent plus « large » que celui du seul EPCI compétent

    * Une [connaissance des besoins](../../../outils/connaissance/) qui se met à jour et qui n’hésite pas à voir loin, quitte à **faire des scénarios**,
    * Des [stratégies d’implantation](../../../outils/strategie_territoriale/index.md) qui sortent du modèle classique ZAE,
    * Des **choix partagés par les territoires** et avec les acteurs qui exploitent au mieux **les atouts de chacun**,
    * Une **traduction effective de ces choix dans les décisions** et communications de tous.

## Programmation et conception des ZAE
Une fois localisée, une ZAE fait l’objet d’un travail de conception qui peut être déterminant pour optimiser l’usage du foncier. A la fois par sa complexité, et par sa gouvernance, cet exercice souffre souvent de certains manques.

Une **organisation parcellaire** des ZAE qui favorise la sous-densité :

* Des trames parcellaires standardisées et en général surdimensionnées, sans prise en compte des besoins réels des entreprises ;
* Une trame souvent rigide ne permettant pas d’adapter les parcelles aux besoins spécifiques des preneurs ;
* Les potentiels de subdivision des parcelles sont mal anticipés, bloquant les potentiels de densification ultérieurs.

Des **principes d’aménagement** peu efficaces :

* Des partis-pris d’aménagement et des normes qui génèrent des espaces non-cessibles importants (dimensionnement des espaces publics, place des espaces verts, ouvrages hydrauliques …) et limitent donc l’efficacité foncière ;
* Des opportunités de superposition/mutualisation de fonctions sur un même espace qui ne sont pas explorées dans une logique d’économie de la fonctionnalité voire d’écologie industrielle (ex. construction sur plusieurs niveaux, mixité des espaces, stationnement en commun …) ;
* Une sous-exploitation des infrastructures multimodales existantes.

Des **formes bâties et implantations** peu efficaces :

* L’existence de contraintes réglementaires qui imposent parfois des implantations en milieu de parcelle, limitant les potentiels de densification ultérieurs ;
* Une logique de commercialisation à la parcelle, sans réflexion d’ensemble sur le bâti, qui conduit aussi à une implantation peu organisée des bâtis les uns par rapport aux autres, sans recherche d’économies d’espaces ;
* Une réglementation limitant la constructibilité et potentiellement génératrice de sous-densité (multiplication des couches de contraintes, règles d’implantation…) ;
* Les choix d’implantation des bâtiments, avec une recherche de visibilité depuis les axes routiers, qui engendrent une multiplication des bandes inconstructibles ;
* Des formes bâties standardisées peu efficaces. Des construction à plat, une absence d’étages, des espaces non modulables, peu de réflexion sur la mutabiité future voire la déconstruction de ces bâtiments etc.

Des zones d’activités aménagées prioritairement pour la **circulation routière** :

* Un surdimensionnement des voiries principales afin de répondre à une évolution théorique de la zone qui a de nombreux impacts ;
* Une réflexion sur la mobilité et l’accessibilité qui se réduit aux seuls véhicules motorisés, au détriment des formes de mobilités douces (piétonne, cycliste…) ;
* Un manque de raccordement des zones aux réseaux de transports en commun provoquant la multiplication d’espaces dédiés au stationnement ;
* Peu d’élaborations de Plan de Déplacement d’Entreprise ;
* La place trop importante accordée à la voiture au sein de la zone et l’absence de solutions alternatives en matière de mobilité ont des impacts sur la « vie sociale », la qualité environnementale et l’efficacité foncière des ZAE.

!!! idee "Quelques pistes pour améliorer la situation"
    Nous devons innover en matière de design et de gestion foncière

    * **Anticiper le caractère vivant des ZAE**, prévoir des **capacités d’évolution** dans le temps **des parcelles et des bâtiments**,
    * Prévoir **plus d’espaces pour des fonctions mutualisables** au sein des zones pour les services dont tous ont besoins,
    * Mieux exploiter les possibilités de **superposition des fonctions**,
    * La conception doit reposer sur une logique de [cycle de vie de la ZAE](scenarios.md) et de **coût global** intégrant les futures étapes de **requalification voire de renaturation**.

## Processus de commercialisation
La commercialisation est souvent la première étape dans la vie d’une ZAE où les besoins des entreprises sont directement exprimés. Un temps qui pourrait être mieux mis à profit pour optimiser l’usage des sols, alors que des difficultés importantes apparaissent.

Un **mécanisme de production du foncier économique** structurellement générateur de sous-densité :

* Le processus d’aménagement des ZAE est centré sur la production quantitative de foncier à vocation économique et non sur sa performance réelle ;
* Les acteurs (collectivités, aménageurs, développeurs économiques…) sont incités à une commercialisation rapide du foncier sans regard sur son « efficacité foncière » à savoir la bonne entreprise au bon endroit, tenant compte de ses besoins réels, et des synergies positives possibles avec son environnement.

Un **prix du foncier économique** faussé :

* Le prix de vente du foncier économique reflète rarement les coûts économiques réels (coût d’achat du foncier + coût d’aménagement + frais financiers + ingénierie) ;
* Les acteurs de l’aménagement soutiennent parfois l’activité économique et rivalisent entre eux par des prix bas afin d’attirer les projets d’implantation ;
* Les mécanismes de marché contribuent donc à l’acquisition de parcelles de tailles supérieures aux besoins et provoquent une surconsommation foncière.

Des **stratégies d’entreprises** potentiellement génératrices de sous-densité :

* Des entreprises dont les intérêts réels ou supposés peuvent représenter des freins à la densification : image de l’entreprise, réticences à construire en hauteur… ;
* Des stratégies patrimoniales des entreprises et entrepreneurs qui poussent à l’acquisition de fonciers trop importants et souvent inutiles (volonté de constituer des réserves foncières) ;
* Une organisation fiscale qui fait souvent privilégier l’acquisition à la location par les PME, associant aux besoins fonctionnels de l’entreprise des enjeux d’investissement immobilier, qui faussent les programmes.

!!! idee "Quelques pistes pour améliorer la situation"
    Nous devons innover en matière de forme juridique d’implantation de l’entreprise sur un foncier

    * L’attribution d’un foncier doit reposer sur un **principe de « sobriété »** de la consommation foncière et de **« juste nécessaire » au regard des besoins** de l’activité économique,
    * Le foncier doit **passer du statut de patrimoine privé à celui d’atout commun**,
    * Le modèle « j’achète > j’aménage > je vends » rend les optimisations très complexes. **Le principe de transfert de la propriété** du sol à un acteur privé **ne doit pas systématiquement être la norme**.

## Vie et gouvernance des ZAE
Après sa mise en œuvre, la vie d’une ZAE pourrait aussi permettre de proposer des modes d’optimisation de l’usage foncier. Un certain nombre de freins existent pourtant.

L’apparition progressive d’espaces privés **délaissés et difficilement mutables** :

* Les opérations de reconversion des bâtiments et parcelles privés ne donnent pas toujours une plus-value aux propriétaires privés permettant d’inciter à la valorisation d’espaces ou de bâtiments sous-utilisés ;
* Il manque fréquemment d’une anticipation des besoins de gros entretiens et de réhabilitation/requalification même à long terme dans les opérations d’aménagement.

Le manque de **gouvernance collective** des ZAE associant toutes les parties prenantes, permettant de valoriser les potentiels de densification :

* Les entreprises qui s’implantent sur une ZAE participent de facto à un collectif, mais le plus souvent sans en avoir pleinement conscience ;
* Au-delà de la cession foncière, les acteurs de l’aménagement ne restent plus les interlocuteurs des occupants en tant que « gestionnaire » global et la relation se réduit souvent à l’entretien des espaces publics ;
* Le processus de la conception à l’exploitation d’une ZAE intègre trop peu ses occupants dans un dispositif global de gouvernance partagée ;
* De nombreux acteurs, chacun avec sa propre légitimité, proposent des solutions d’animation, de coopération ou de coordination. Ces « offres de services » se font souvent dans l’ignorance des autres initiatives ;
* L’animation d’une ZAE est utile à chaque étape de son cycle de vie et sert de base lorsqu’une concertation devient utile, notamment lors des phases d’extension ou de requalification ;
* Les ZAE sont trop souvent des espaces banalisés au sein desquels les services de proximité, les liens sociaux sont déficients par manque de dialogue.

!!! idee "Quelques pistes pour améliorer la situation"
    Nous devons animer et faire vivre nos zones d’activités

    * Les espaces d’activités peuvent **dialoguer entre eux** et constituer le volet évolutif des stratégies au travers de **réseaux d’acteurs**,
    * Les ZAE qui disposent de [fonctions d’animation](../../../outils/vie_ZA/gestionnaire_animateur) résistent mieux au temps, leur valeur foncière aussi. Il faut trouver les modes d’animation adaptés le permettant.

## Valorisation de la ressource foncière
Plus largement, les ZAE s’inscrivent dans des politiques urbaines qui trop souvent laissent peu de place au développement de stratégies foncières inscrites dans le long terme.

Une méconnaissance [des potentiels fonciers en renouvellement urbain](../../../references/guides/evaluer_potentiel_foncier-Norm-2018.md) :

* La priorité est donnée à la commercialisation de ZAE récentes ;
* Les potentiels fonciers en secteur urbanisé privé sont méconnus ;
* Le potentiel des friches est sous-exploité ;
* Un déficit d’anticipation de l’obsolescence des ZAE pour les requalifier et les maintenir dans les « critères de marché »
* Une propension à consommer des nouveaux espaces « vierges » plutôt qu’à agir en reconquête/renouvellement d’espaces déjà urbanisés.

Une logique **systématique de cession foncière** à réinterroger :

* Les intercommunalités, qui sont les acteurs principaux de la gestion des ZAE, alimentent une pratique continue d’aménagement de foncier économique ;
* Le processus d’urbanisation et d’aménagement des ZAE se conclut presque systématiquement par une cession de la maîtrise foncière privant la collectivité de tout levier d’action par la suite ;
* Le développement économique est insuffisamment réfléchi par les acteurs de l’aménagement comme un processus global ayant des impacts à long terme nécessitant leur implication en continu ;
* Les intercommunalités rencontrent de réelles difficultés pour racheter le foncier non-bâti ou les friches aux propriétaires privés (montant financier, refus de cession, manque de dialogue au fil de l’eau entre entreprises et collectivité…).

Un **foncier privé** sous-utilisé :

* L’organisation de la production et de la commercialisation génère une inflation des surfaces acquises par les entreprises ;
* La densification par division n’est pas perçue comme un potentiel à valoriser par les entreprises en raison de la faiblesse du prix du foncier et de la complexité de mise en œuvre ;
* Les règles d’urbanisme peuvent contraindre la densification ;
* La perte de la maîtrise foncière par les acteurs de l’aménagement, lors de l’implantation des entreprises en ZAE, les oblige à devoir acquérir en permanence de nouveaux espaces agricoles afin de les aménager pour répondre à la demande.

!!! idee "Quelques pistes pour améliorer la situation"
    Nous devons innover en matière de spatialisation des activités économiques

    * La décision du **lieu d’implantation d’une activité** économique doit être pensée par la Collectivité au regard d’une **approche globale inclusive des paramètres de mobilité**, de vitalité des territoires notamment ruraux, de logement… qui dépasse la logique d’intérêt privé,
    * **L’artificialisation** des Espaces Naturels Agricoles et Forestiers **doit être un « dernier » recours** après que les solutions de **reconquête de friches**, de densification, d’implantation en tissu urbain etc. se sont avérées inadaptés ou impossibles.    
