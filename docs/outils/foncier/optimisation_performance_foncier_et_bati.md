title: Optimisation de la performance du foncier et du bâti

La consommation de foncier agricole pour constituer de nouvelles ZAE est une réponse qui répond à un besoin réel des entreprises : celui de trouver sur le marché du foncier aménagé pour créer, déplacer ou faire grandir leur activité.

Cependant, la proportionnalité de cette réponse et son efficacité doivent être questionnées au regard des usages réels. D’une manière générale, la plupart des observations effectuées permettent de constater que l’emprise au sol moyenne du bâti sur la parcelle est souvent inférieure à 30%, le reste étant occupé par les aires de stationnement, les espaces libres, les aires de stockage et les espaces végétalisés. Il est également rare de voir des bâtiments en hauteur en ZAE.

Cette faible optimisation peut être la résultante de différents processus lors de la conception et la commercialisation, ou de la vie des entreprises :

* Un parcellaire surdimensionné et rigide par sa conception, qui a imposé aux entreprises de faire l’acquisition de parcelles de dimensions trop importantes. Ce cas est notamment fréquent pour les petites PME-PMI aux besoins fonciers limités, alors que l’aménagement de très petites parcelles nécessite des coûts d’aménagement parfois très supérieurs.
* Une commercialisation tournée uniquement sur les enjeux d’attractivité, et peu regardante sur les besoins réels des entreprises. Il en résulte fréquemment des prix de cession bas voire symboliques, souvent en dessous des coûts de revient, qui poussent les entreprises à surdimensionner leurs parcelles.
* La constitution de réserves foncières privées, en général à des fins d’extension à moyen/long terme de l’entreprise voire à des fins spéculatives. Il est complexe pour les entreprises d’évaluer leurs besoins éventuels d’extensions futures. Faute d’enjeux financiers réels avec des prix de foncier bas, les réserves foncières sont en général largement surdimensionnées, et parfois pas exploitées après de longues périodes de temps.
* Des fonciers acquis, mais qui n’ont pas fait l’objet de construction.
* La réorganisation des occupations de deux entreprises voisines peut aussi permettre de libérer des emprises potentiellement cessibles, notamment par la mutualisation d’espaces de stationnement.
* Des règles d’urbanisme ou des normes qui limitent les capacités d’usages et face auxquelles on n’a pas su optimiser l’usage des espaces non-bâtis en superposant les fonctions « annexes » à l’activité économique (stationnement, gestion de l’eau, trames verte et bleue…).

Ces emprises non-bâties présentes dans la plupart des ZAE existantes, pourraient être mises sur le marché et constituer une alternative à l’étalement urbain induit par la constitution de nouvelles ZAE. Cela permet en outre d’amener de nouvelles activités sur la zone et une certaine émulation ou de nouvelles synergies économiques. Ou encore d’améliorer la qualité de vie par la création de services, de rapporter un revenu d’activités complémentaire pour certaines entreprises.

En s’inspirant des démarches [BIMBY](http://www.wikibimby.fr/index.php/La_marque_collective_open_source_BIMBY) (_Build In My BackYard_) engagées sur le résidentiel pavillonnaire, on peut initier des divisions foncières permettant de mettre sur le marché des parcelles privées dans des ZAE existantes ou réfléchir différemment à la conception du bâti dans les espaces d’activités. Cela permet de dégager une offre foncière et immobilière, en ZAE, en secteur aménagé, pour des coûts d’investissements réduits et sans artificialisation de sol complémentaire.

## Mise en œuvre
L’objectif étant d’initier des cessions dans les ZAE existantes sur du foncier déjà commercialisé, il s’agira d’opérations privées. Elles pourraient parfaitement être réalisées à la seule initiative des propriétaires dans bien des cas, mais nécessitent à l’évidence une forme d’initiation par le public, en particulier si l’on veut optimiser les résultats.

Pour sortir d’une vision en deux dimension, il peut être opportun de développer une réflexion sur la verticalité de la ZAE. Notamment si la collectivité souhaite créer un centre de vie plus intense, marquer un repère pour son territoire et donc si cela répond à un enjeu stratégique du projet de territoire, il faut que le règlement d’urbanisme permette d’adopter des constructions en étages[^caue44].

### Initier la démarche
Mise en place d’une organisation projet interne Collectivité ou externalisées à un prestataire. Une démarche de densification peut être amorcée en complément d’interventions sur les espaces publics d’une ZAE, mais peut aussi être organisée sur toutes les ZAE qui présentent un potentiel commercial réel.

### Identifier les parcelles
Le repérage des parcelles à potentiel de densification (non-bâties, sous occupées, vacantes ou en friche) passe à la fois par un repérage cartographique (le ratio bâti / surface de la parcelle est un premier indicateur de repérage, avant analyse de l’usage de la parcelle sur orthophotographie) et une vérification terrain et cadastrale.
Un travail avec les entreprises, par le biais d’un questionnaire, si possible dans le cadre d’une mission d’animation ou de gestion de la zone, peut être une bonne façon d’appréhender certaines stratégies d’entreprises tout en engageant le dialogue avec elles.

### Provoquer l’engagement
Le contact avec le propriétaire permet de cerner les enjeux spécifiques de celui-ci (occupant ou non, proximité de la retraite, enjeux fiscaux…), les potentiels réels de densification et d’expliquer l’intérêt de la démarche.

### Accompagner l’exercice
La mise en œuvre de l’opération passe par une division foncière, la viabilisation du lot créé (réseaux, accès…) puis la cession et la réalisation du projet de construction de l’acquéreur. La Collectivité est directement impliquée par différentes étapes du processus, mais peut aussi jouer un rôle de facilitateur global de la démarche.

## Recommandations
Le rôle de la Collectivité est théoriquement d’initier le processus privé, avec pour objectif d’offrir une réponse foncière complémentaire à la demande des entreprises sans création de nouvelle ZAE, ou en prolongeant la durée de commercialisation de celles qui le sont encore. Cependant, deux obstacles importants peuvent freiner ce processus et nécessiter une implication plus importante de la Collectivité :

* Le vieillissement de certaines ZAE réduit considérablement leur potentiel de commercialisation. Dans ce cas, la mise en œuvre d’une opération de réhabilitation globale portant notamment sur les espaces publics peut être un préalable nécessaire à l’amorçage d’une dynamique privée de densification,
* Le prix de marché et la complexité des opérations de viabilisation peuvent induire que dans certains cas le bénéfice attendu de la cession par le propriétaire soit très réduit voir nul. La prise en charge partielle ou la réalisation de la viabilisation par la Collectivité peut avoir dans ce cas un effet levier important,
* L’usage des espaces publics ne doit pas être négligé, ni au stade de la conception, ni dans le cadre d’une nouvelle organisation. Les voiries notamment prennent souvent une part non négligeable dans la consommation de l’espace en ZAE, avec des délaissés, des bandes inconstructibles le long des voies sans utilité et cher à entretenir. Pour autant, il est possible, en le justifiant, de diminuer les reculs imposés par la loi Barnier. Il est également envisageable de les augmenter pour donner une utilité à ces espaces (trame verte, espaces agricoles…).

## Outils juridiques et financiers mobilisables
Des divisions parcellaires avec cessions et/ou locations (avec droits réels ou non) sont envisageables pour que les propriétaires privés fassent participer leurs terrains à une opération de rationalisation de l’occupation des terrains. A ce titre, aucun outil juridique n’est meilleur qu’un autre s’agissant des droits et obligations qu’il impose aux différentes parties. Leurs avantages et leurs inconvénients seront différents selon les choix patrimoniaux des propriétaires.

Si plusieurs propriétaires sont concernés par une opération de remembrement, ils pourront être incités à constituer une association foncière urbaine[^afu]. Cette association sera créée pour les besoins de l’opération de travaux, mais pourra être maintenue pour la gestion des ouvrages communs ainsi réalisés.

## Références

* [Evaluer le potentiel foncier dans le cadre du PLUi](../../references/guides/evaluer_potentiel_foncier-Norm-2018.md) - CCI, CA, DREAL, Région, SAFER et EPF Normandie, DDTM de Seine-Maritime, PNR Boucles de la Seine, Agences d'urbanisme de Rouen, Le Havre et Caen, CEREMA – juin 2018

[^caue44]: Sur ce thème, nous recommandons la lecture de [« pour en finir avec la zone »](https://www.caue44.com/?portfolio=pour-en-finir-avec-la-zone) - CAUE 44 – novembre 2011 qui présente les résultats d’un concours original.
[^afu]: régime fixé par l’article L.322-1 du code de l’urbanisme et l’ordonnance n°2004-632 du 1er juillet 2004 relative aux associations syndicales de propriétaires
