title: Exemples de mutualisations

## Parking commun à plusieurs entreprises
Une zone d'activité est confrontée à le pratique de stationnement « sauvage », notamment en lien avec des manques de capacités de certaines entreprises. Le foncier disponible, quand il existe, serait plus utile à l'activité économique et les investissements nécessaires pour des parkings en étages paraissent disproportionnés. D'autres entreprises, disposent de suffisamment de terrain pour le stationnement de leurs employés.

**Solutions ou opportunités**

* Développement d’un ou plusieurs parkings communs dans le périmètre de la ZAE
* Intégrer la mutualisation des stationnements dans un ensemble de mesure visant une mobilité plus fluide et durable : covoiturage, transport collectif, mobilité douce…
* Possibilité de combiner la conception d’un parking avec d’autres services (salles de réunion, hôtel, services aux salariés, commerces de proximité), soit la création d’un véritable centre de zone en termes d’activité, de centralité et de mobilité.
* Existence de d’un foncier à valoriser situé en cœur de zone
* Formaliser le stationnement et sanctionner le stationnement « sauvage »

**Faisabilité / complexité / risques**

* Prix de la place de stationnement dans les nouvelles infrastructures et disponibilité à payer des entreprises et/ou des employés
* Distance à parcourir par les utilisateurs jusqu’à leur entreprise dans une zone à faible densité d’emploi et aux voiries peu adaptées aux mouvements piétonniers
* Ne pas considérer que le seul parking résoudra tous les enjeux de mobilité, les autres mesures de report modal doivent être développées conjointement.

## Services de sécurité/gardiennage partagés
Sur une zone régulièrement aux prises avec des problèmes de vols ou de vandalisme, les besoins de sécurité ou de gardiennage sont régulièrement exprimés. Pour autant, chacune des entreprises n'est pas forcémment en capacité de financer les services en question.

**Solutions ou opportunités**

* Sécurité améliorée par une meilleure couverture
* Possible économie d’échelle grâce à la mise en place d’un contrat commun interentreprises
* Service partagé facile à mettre en œuvre servant de catalyseur pour la création d’une association d’entreprises
* D’autres services peuvent être associés dans le cadre d’un accord plus large entre plusieurs entreprises (centrale d’achats, gestion des déchets, etc.)

**Faisabilité / complexité / risques**

* Définir un cahier des charges commun pour un tel contrat
* Consensus sur une clé de répartition financière et le niveau de service
* Relation avec les prestataires existants

**Outils juridiques et financiers mobilisables**

La mise en œuvre d’un tel service commun pourrait être réalisée par la contractualisation avec un prestataire choisi en ayant recours à un mécanisme de type groupement de commande entre les entreprises intéressées.

A ce titre, une convention constitutive du groupement permettrait d’encadrer les conditions de contractualisation avec le prestataire (publicité, mise en concurrence…), les critères de choix et la participation financière des entreprises. Cette convention devra également prévoir les conditions de retrait des entreprises en cours de contrat de prestation pour éviter les discussions ultérieures, voire les conflits[^assu].

[^assu]: La solution de la mise en œuvre d’un mécanisme assurantiel ne paraît pas adaptée, dès lors qu’elle implique un coût supplémentaire qu’il n’est pas possible d’estimer (fonction des propositions des compagnies d’assurances)
