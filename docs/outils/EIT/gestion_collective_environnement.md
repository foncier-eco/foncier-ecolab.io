title: La gestion collective de l'environnement

La protection de l’environnement s’inscrit de plus en plus au cœur des réflexions de développement, y compris sur le plan réglementaire. La dégradation des milieux s’approche d’un niveau tel qu’elle risque de remettre en cause le développement économique.

Dans ce contexte, la démarche environnementale, notamment quand elle est menée dès l’amont du projet, et les coûts financiers associés, sont à évaluer dans une logique d’économie globale pour la collectivité, l’aménageur et les entreprises.

Les bénéfices attendus d’une telle démarche sont nombreux :

* Valoriser le territoire et mettre ses atouts en avant, limiter la consommation d’espaces naturels et agricoles par une réflexion sur l’implantation de la zone dans le paysage, la compacité du plan masse en composant des secteurs plus ou moins denses selon le type d’entreprises prévues ou l’implantation des bâtiments sur la parcelle;
* Préserver la ressource en eau, par une gestion des consommation, une gestion raisonnée des eaux pluviales, le contrôle des rejets ou le choix des essences végétales les moins gourmandes en arrosage;
* Assurer le maintien de la biodiversité par une étude d’implantation de corridors écologiques, des choix adaptés pour les essences végétales dans l’aménagement du quartier et des parcelles ou la mise en place une gestion différenciée des espaces verts;
* Limiter la consommation des ressources par une conception des aménagements extérieurs peu consommatrice en ressources (bordures, limites, etc), des règles favorisant des matériaux durables, nécessitant peu d’entretien (peintures, vernis) ou des éclairages publics autonomes;
* Economiser les énergies par la limitation des déplacements, le covoiturage, les transports en commun et les liaisons douces, l’incitation à l’utilisation d’énergies renouvelables (photovoltaïque, éolien), aux mutualisations pour le chauffage, à la coopération et aux échanges entre les entreprises, (ex : se chauffer avec des déchets…).

C’est à partir d’un diagnostic du site d’implantation que les différents acteurs définissent leurs intentions, pour s’inscrire dans une démarche durable. A partir de ces intentions, différents moyens peuvent être mis en œuvre, à l’échelle du quartier et des entreprises.

## Mise en oeuvre

Partant du constat qu’une faible proportion des ZAE fait un travail sur la qualité environnementale de l’aménagement, l’association Orée propose[^oree] de s’appuyer sur des étapes clés de la vie d’une ZAE, comme la création, l’extension ou la requalification, pour lancer un processus vertueux, résumé ici.

### Du Système de Management Environnemental...
La mise en place d’un système de management environnemental (SME) par les gestionnaires de zones d’activités apparaît comme un élément essentiel de la pérennisation de la qualité environnementale de ces espaces d’accueil d’activités et de réponse aux nouvelles attentes des entreprises et des territoires. Cette démarche va permettre au gestionnaire de structurer ses actions autour de sa politique environnementale par une organisation assurant la prise en compte de l’environnement dans un processus d’amélioration continue à tous ses niveaux de fonctionnement.

De plus, cette démarche permet d’inciter et d’accompagner les entreprises.

### ... à la gestion collective de l'environnement sur les zones d'activités
Or la prise en compte de l’environnement par les entreprises peut difficilement se faire sans développer une gestion collective de l’environnement à l’échelle de la zone d’activités. En effet les PME-PMI n’ont généralement pas les moyens humains, techniques et financiers de mettre en place une politique de gestion environnementale. Ces contraintes amènent à envisager la mise en place de solutions collectives et économiquement viables.

Aujourd’hui, de telles démarches se développent largement, notamment dans le domaine de la gestion des déchets industriels.  Elles permettent de développer des synergies interentreprises et de générer des économies d’échelles par une approche commune.

Les zones d’activités, regroupement d’entreprises sur un même espace géographique, facilitent la mise en place d’une telle démarche. Des démarches de gestion collective peuvent très bien porter sur des territoires plus larges, comme une région. Dans ce cas, il s’agit le plus souvent d’actions par secteur d’activités.

La gestion collective de l’environnement peut être ainsi définie comme une approche commune entre divers acteurs de l’environnement à la recherche de solutions pour permettre :

* de générer des économies d’échelles;
* d’intégrer à un moindre coût une gestion environnementale; 
* de mettre en place des solutions optimisées et adaptées aux besoins des utilisateurs; 
* de trouver des solutions difficilement abordables de façon individuelle; 
* de permettre aux PME-PMI d’allier développement économique et minimisation des impacts environnement;
* de mutualiser les moyens et les coûts, développer des synergies, créer une autre culture d’entreprise… par exemple.

## Recommandations

En se basant sur les exemples existants, et compte tenu des difficultés fréquemment rencontrées (mobilisation des entreprises, complexité des montages financiers ou de l’obtention des aides, technicité du sujet) certains éléments sont intéressants à intégrer pour une telle démarche.

* L’implication des acteurs locaux est essentielle, ne serait-ce que pour initier la démarche et créer la dynamique;
* Profiter de réflexions sur la réhabilitation de la zone, son agrandissement, l’amélioration des services, des besoins collectifs exprimés par des PME, PMI qui ne peuvent y répondre seules ou encore sur l’utilisation d’équipements collectifs comme une station d’épuration pour lancer la démarche;
* S’appuyer sur les liens et réseaux existant : associations d’industriels, présence d’un gestionnaire de zone, d’un organisme pilote d’un animateur permanent, acteurs locaux de l’environnement;
* Faire intervenir un spécialiste en gestion environnementale, par exemple par le biais d’un groupement d’employeurs comme les CEPIE (Centre d’Emploi pour la Performance et l’Information Environnementale) ou par un emploi en temps partagé;
* Encourager la participation pour l’élaboration des cahiers des charges, et la construction de solutions techniques diverses et flexibles des solutions techniques à retenir;
* Implication et accompagnement des démarches par les acteurs locaux de l’environnement.

## Outils juridiques et financiers mobilisables
Il n’existe aucun cadre juridique spécifique ou obligatoire, ce qui permet une grande flexibilité pour la sécurisation contractuelle du projet.

* Il est impératif d’établir une documentation juridique et contractuelle dédiée pour garantir l’efficience et la pérennité du projet
* L’expérience montre qu’il est plus aisé de construire ce corpus de règles sur plusieurs niveaux (avantage d’être didactique et d’organiser les rôles et obligations de chacun des partenaires) :
    * une charte générale qui fixe les politiques globales, des objectifs quantifiés, et scelle l’accord des volontés à œuvrer en ce sens;
    * un contrat (type contrat de partenariat) entre les divers opérateurs sur la zone, qui fixerait les responsabilités générales, la réparation des coûts, les obligations par domaines/typologie (ex : déchets, gestion de la ressource en eau, économie du foncier, etc…)
    * un contrat de gestion de zone qui définirait pour chaque opérateur/ intervenants sur la zone, ses obligations précises, ses responsabilités, les coûts assumés, etc (ex : contrats de gestion de sites sur les plateformes industrielles)
* Cibler les activités autorisées/ non autorisées sur la zone permet de limiter le risque d’atteinte à l’environnement. La présence d’une ICPE n’est pas nécessairement rédhibitoire car bien qu’elle soit susceptible d’avoir des impacts sur l’environnement, elle est par ailleurs censée respecter un corpus de règles qui éloigne significativement ce risque. En revanche d’autres activités non classées ICPE peuvent comporter davantage de risque (station-service, garage, ateliers de peinture, entrepôts de stockage etc…)
* Il conviendra alors d’intégrer cette restriction à l’établissement de certaines activités dans la charte qui sera signée par les copropriétaires. Il conviendra de veiller à ce que cette prise de position soit expressément justifiée dans la charte pour des motifs d’intérêt général.

[^oree]: [Gestion environnementale des zones d'activités](http://www.oree.org/gestion-environnementale-ZAE.html)
