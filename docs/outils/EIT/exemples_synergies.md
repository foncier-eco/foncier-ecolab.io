title: Exemples de synergies possibles

## Mutualisations énergétiques
### Rejets thermiques et réseaux de chaleur
Une zone est située en marge des réseaux de chaleur existants mais comporte plusieurs entreprises motivées pour un approvisionnement mutualisé en énergie de chauffage. Un nombre réduit d’activités économiques à forte consommation d’énergie sont présentes dans la zone qui dispose par ailleurs de rejets thermiques industriels à valoriser.

**Solutions ou opportunités**

* Développer les opportunités de réseaux de chaleur;
* Collecte et valorisation des rejets de chaleurs des entreprises existantes par l’intermédiaire d’un réseau de chaleur.

**Faisabilité / complexité / risques**

* Connaissance insuffisante des besoins et rejets énergétiques des entreprises de la zone (puissance, consommation, variations temporelles – notamment saisonnières);
* Coordination nécessaire au-delà du périmètre de la zone.

### Toitures Photovoltaïque
Sur une zone comportant d'importantes surfaces de toitures, on identifie des consommations électriques massive. Par ailleurs, les entreprises concernées voient un intérêt pour l'investissement dans les énergies renouvelables.

**Solutions ou opportunités**

* Création d’installations solaires photovoltaïques sur les toitures;
* Rentabilité de ce type d’investissement;
* Possibilité de créer des pools d’investissement interentreprises.

**Faisabilité / complexité / risques**

* Conditions contractuelles des partenariats (location de toiture, amortissement…);
* Conflit potentiel d’usage des toitures (densification, agriculture urbaine…).

## Echanges de flux de matière
Il n’est pas rare que sans que les entreprises d’un même territoire le sachent, elles aient la possibilité, soit de valoriser leurs déchets ou co-produits auprès d’entreprises proches géographiquement ou inversement qu’elles disposent à proximité d’une source de matière première dans les déchets ou co-produits d’une entreprise locale.

**Solutions ou opportunités**

* Valorisation des déchets ou co-produit des entreprises;
* Construction de circuits d’approvisionnement résilients aux fluctuations des marchés extérieurs;
* Diminution potentielle des espaces de stockage nécessaires.

**Faisabilité / complexité / risques**

* Nécessité d’échanges de connaissances sur les flux entrant et sortant des entreprises;
* Relation de confiance à instaurer pour ces coopérations autour de données souvent sensibles.

## Mutualisation de la logistique
### Optimisation logistique et circuits courts
Sur une zone déjà sujette un à trafic routier important, certaines entreprises peuvent avoir des transports _à vide_ soit au départ, soit au retour. Ces camions contribuent à l'encombrement, sans plus value économique. Il peut dès lors être intéressant de favoriser les circuits courts et la mutualisation du service de transports par une meilleur connaissance de l'offre et des besoins des entreprises voisines.

Les bénéfices de la mutualisation sont une réduction du trafic de camion et la réduction de frais de transport. Sur le plan foncier, cela peut parfois permettre d’envisager la réduction des espaces dédiés à la logistique ou d’optimiser le stockage.

**Solutions ou opportunités**

* Mutualisation des transports : un camion qui part chargé et revient vide peut être utilisé par une autre entreprise pour autant que les conditions techniques et sanitaires soient respectées;
* Mettre en place une plateforme de coordination interentreprises pour la mutualisation des transports sous la forme d’un groupe de travail ou d’un outil informatique;
* Valorisation de plusieurs bons exemples existants à faire connaître et à répliquer.

**Faisabilité / complexité / risques**

* Les démarches de mutualisation de la logistique sont parmi les plus complexes à mettre en œuvre. Elles impliquent des échanges d’informations cruciales entre les entreprises et un niveau de confiance particulièrement important;
* La complexification de la logistique (horaires, type de véhicule/benne, etc.);
* La compatibilité des produits transportés (p.ex. déchets vs. sable, ou types de produits alimentaires nécessitant des températures différentes;
* L’optimisation des trajets fait partie du métier des transporteurs. C’est à eux de décider des modalités de mise en œuvre.
