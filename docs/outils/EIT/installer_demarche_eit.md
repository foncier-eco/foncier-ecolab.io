title: Installer une démarche EIT

Le contexte de raréfaction de la ressource foncière et de prise en compte croissante des principes de développement durable par les entreprises du territoire, est une opportunité pour les collectivités de faire évoluer son parc de ZAE vers le modèle d’éco-parcs, pour une optimisation de ses espaces de production et une augmentation de l’efficacité de ses entreprises.

La démarche d'Ecologie Industrielle et Territoriale vise à optimiser l’organisation et les équipements dédiés à l'activité économique. Elle peut passer par une réflexion sur la [mutualisation de moyens, services et fonctions](./exemples_mutualisations.md) qui se fera souvent plutôt à l'échelle de la zone. L'objectif étant de tendre vers la construction de véritables [synergies entre les entreprises](./exemples_synergies.md) qui doivent souvent se construire à des échelles plus larges, entre zones d'activités.

Dans tous les cas, les bénéfices attendus sont une amélioration de l'efficacité des entreprises, de la qualité de vie au sein des zones d’activités et une diminution des impacts et besoins en ressources.

La traduction concrète, sous forme d'_éco-parcs_ par exemple, peut se faire de multiples manières : Aménagement de parkings communs, Plan de mobilité inter-entreprises, Parc photovoltaïque mutualisé, Contrat mutualisé de surveillance, Coordination pour la gestion des déchets (éco-points, contrats de collecte, etc.), Services mutualisés (multi-accueil pour jeunes enfants, conciergerie, point de restauration...), Réseau de chaleur, …

## Mise en oeuvre

En général, la mise en place d'une démarche d'EIT poursuit trois grands objectifs

* Doter le territoire d’une stratégie d’écologie industrielle, en lien avec la [stratégie territoriale](../strategie_territoriale/index.md) et la faire vivre
* Engager une réflexion en faveur de la densification des zones d’activités
* Favoriser les synergies entre les entreprises d’une même zone et la mutualisation de services

Plus finement les objectifs fixés dépendront

* de **la vocation** de la zone d’activités (industrielle, artisanale, logistique, commerciale, tertiaire…)
* de son **niveau de spécialisation** (degré de concentration d’entreprises partageant un même secteur d’activités, ex. aéronautique, agroalimentaire, numérique…)
* du **nombre d’entreprises** implantées
* de la **taille des entreprises**
* de son **ancienneté**,
* ...

Il n'y a pas de règle précise pour le déroulement de la démarche, mais les retours d'expérience invitent la mener suivant quatre étapes. L'existence d'un dispositif de [gestion de la zone](../vie_ZA/gestion_ZA) et en particulier d'[animation](../vie_ZA/gestionnaire_animateur) est très favorable pour la conduite de la démarche.

### Préparation et lancement
La préparation pose les bases de la démarche. Elle doit permettre d’établir le contact avec les acteurs-clés (entreprises « ambassadrices » de la démarche), d’anticiper les enjeux qui doivent être traités au niveau de la zone, et de mettre en place les mesures qui assureront l’efficacité de la démarche.
Les éléments-clés de cette phase de lancement sont :

* Susciter l’intérêt auprès des entreprises : assurer une participation élevée et bonne acceptation de la démarche;
* Identifier des alliés stratégiques qui pourront soutenir la démarche et convaincre, le cas échéant, les entreprises moins convaincues;
* Anticiper sur d’éventuelles sources de tension « annexes » à la démarche et y répondre en conséquence (pour diminuer le risque de conflits lors des premières rencontres).

Elle est constituée des activités majeures suivantes :

* Diagnostic de la ZAE, état des lieux des entreprises et des projets;
* Caractérisation des flux de matières, des usages, des besoins de service – et par extension, collecte des données associées;
* Mise en place d’un groupe de pilotage (composé des « ambassadeurs », d’un noyau d’acteurs facilitateurs de la démarche, chargé de définir le projet de la zone et d’en fixer le planning);
* Prise de contact et rencontre de l’ensemble des entreprises de la zone à une phase d’ateliers de co-construction.

### Rencontre(s) inter-entreprises
Avec le mot d’ordre de "flexibilité", cette étape de la démarche inter-entreprises est constituée de 1 à 2 rencontres (en cas de besoin) avec les entreprises de la zone. Les objectifs de ces rencontres sont les suivants :

* Expliquer la démarche, présenter la qualité de la zone, renforcer son identité;
* Permettre aux différents acteurs de se présenter et faire connaissance;
* Amorcer une dynamique de dialogue et collaboration entre ces acteurs et construire la confiance dans le groupe;
* Identifier des problématiques et besoins prioritaires ou sous-jacents nécessitant une réponse ciblée à traiter en-dehors du contexte des ateliers _éco-parc_;
* Identifier les pistes de collaboration et d’écologie industrielle à développer lors des prochains ateliers.

Cette rencontre est l’occasion :

* De présenter aux entreprises le diagnostic de la zone, et les enjeux identifiés par le comité de pilotage;
* De lancer les différents sujets sur les thèmes que les différents participants souhaiteront aborder dans le cadre de la démarche _éco-parc_. L’équipe de projet procède au recensement des besoins et pistes d’opportunités : sont ainsi recensées les questions qui auront besoin d’approfondissement, et les pistes de travail à creuser lors des ateliers suivants;
* De répondre aux diverses questions des participants sur la démarche.

L’équipe de pilotage à l’issue de cette rencontre élabore un catalogue des besoins et pistes d’opportunités. Ce catalogue va permettre d’orienter les ateliers à venir et constitue également les premières pistes de projets d’écologie industrielle qui pourront être concrétisés.

### Atelier(s)
Ce deuxième volet de la démarche avec les entreprises de la zone prend la forme concrète de 1 à 2 ateliers de travail. Les objectifs de ces ateliers sont les suivants :

* Réaliser des groupes de travail thématiques en fonction des besoins identifiés ;
* Initier des pistes de projets inter-entreprises ;
* Etablir un plan d’actions définissant les étapes de mises en oeuvre et les rôles de chacun.

Ces ateliers se déroulent habituellement en 3 temps :

1. L’atelier débute avec la **présentation des thèmes retenus** (généralement entre 2 et 6) sur la base du catalogue des besoins établi précédemment. Il est intéressant de favoriser, voire systématiser autant que possible l’intervention d’experts1 dans une perspective de sensibilisation/formation. Cela permet notamment de rendre compte des actions menées et des procédures spécifiques au territoire, pour chaque thématique.
1. Afin de faciliter la discussion, des **groupes de travail thématiques** sont formés. Chaque participant choisit un groupe de travail, en fonction de ses affinités et centres d’intérêt. Ces groupes à effectif réduit sont l’occasion de travailler de manière plus approfondie et de favoriser l’échange.
    * Les participants pourront participer à environ 2 ou 3 groupes de travail pendant la durée de l’atelier, afin de faire un tour d’horizon des différentes thématiques et de s’engager dans un ou plusieurs projets de collaboration inter-entreprises (écologie industrielle).
    * L’animation du groupe est assurée par un membre de l’équipe d’animation au moyen d’un poster (solutions / actions / responsabilités / planning). Les groupes de travail suivent les étapes de travail suivantes :
        * Complément / réactions sur le constat, la problématique présentée au point 1 (rapide) ;
        * Solutions proposées et actions pour la mise en œuvre ;
        * Répartition des rôles : engagement des entreprises, de la collectivité animatrice de la démarche, etc. ;
        * Plan d’action synthétisant les principales étapes à respecter, les acteurs concernés et leur rôle ainsi que les délais envisagés La synthèse est effectuée par un rapporteur-participant désigné par le groupe ;
1. A l’issue des travaux de groupes, les différentes **pistes et plans d’actions élaborés pour chaque thématique sont mis en commun et discutés en plénière**. Les prochaines étapes sont clairement explicitées. Les plans d’actions visent un engagement des acteurs pour leur réalisation. Ceci peut passer par une confirmation de l’inscription des participants dans les différents groupes de travail. S’il semble encore difficile d’établir le plan d’actions à l’issu de l’atelier, notamment par manque de temps, un deuxième atelier est alors organisé. Il reprendra la réflexion thématique en groupes de travail pour aboutir aux plans d’actions détaillés.

### Suivi de la mise en œuvre des actions

Un suivi efficace est nécessaire pour s’assurer de la mise en place des actions définies au cours des différentes rencontres. Il doit permettre de soutenir la mise en route d’un processus qui à terme devrait être porté par les entreprises de la zone.

Une communication efficace et l’investissement des acteurs importants de la zone sont fondamentaux pour la réussite de cette étape qui devrait aboutir à l’émergence d’une véritable communauté d’entreprises de la zone qui installe une dynamique de collaboration, coopération et innovation.

L’animateur de la démarche a la charge du suivi et de la relance des groupes de travail créés lors des ateliers, et en particulier :

* De convoquer et animer les groupes de travail pour la mise en oeuvre des plans d’action jusqu’à la réalisation d’un projet
* De communiquer les résultats des groupes par courriel de façon régulière
* De convoquer les entreprises en plénière annuelle pour maintenir la dynamique d’échange et faire émerger de nouveaux projets

Selon la thématique et en fonction du degré de maturité du groupe de travail, le pilotage peut être délégué complétement à une autre entité (entreprise, association d’entreprise, commune ou autre).

## Recommandations
La compétence de développement économique est répartie entre le niveau régional et le niveau intercommunal ce dernier ayant directement la charge de la gestion des ZAE. Donc même si l’impulsion de la démarche est issue d’une stratégie régionale, sa mise en œuvre opérationnelle reposera sur une intercommunalité qui devra dégager des moyens notamment humains pour assurer si ce n'est la conduite du projet au moins un lancement très actif de la démarche.

Quel que soit le pilote, une vigilance importante sur le risque d'essouflement est indispensable. Maintenir un rythme suffisant d'échanges, de rencontres, de mise en valeur des aboutissements sont autant d'éléments auxquels doit veiller l'animateur de la démarche.

Afin d’en fiabiliser la réussite, il est préférable de lancer le processus d’évolution vers le modèle d’éco-parc (écologie industrielle, économie circulaire…) par une opération pilote à l’échelle d’une seule ZAE. Cette expérience aura pour vocation de poser les bases du dispositif avant son déploiement sur d’autres ZAE.

Enfin, un élément fondamental de réussite est l’instauration d’un climat de confiance entre l’ensemble des parties-prenantes. Au cours du processus, chacun est amené à mieux comprendre son fonctionnement au sein de l’ensemble productif local, mais aussi à livrer des éléments, parfois précis, sur ses propres fonctionnements et processus. Pour que cette mécanique se mette en œuvre, les garanties d’une confidentialité minimale, d’une véritable recherche de bien commun sont à poser.

## Références

* [L'EIT, un outil au service des territoires](../../references/syntheses/eit_synthese_ademe-2018.md) - ADEME - Décembre 2018
* le [Programme national de synergies interentreprises](https://www.ademe.fr/programme-national-synergies-interentreprises) piloté par l'ADEME a permis de mener des expérimentations très pragmatiques sur la mise en oeuvre de synergies ou de micro-synergies.
* avec [la toile industrielle® de l'Estuaire de la Seine](https://www.aurh.fr/prospective/toile-industrielle) l'Agence d'urbanisme du Havre a conduit un très gros travail de conceptualisation des écosystèmes sur son territoire.

[^exp]: Exemples d’experts à convier : responsable de voirie communale pour la gestion des déchets, spécialiste de plan de mobilité interentreprises ou porteur de projet énergétique.
