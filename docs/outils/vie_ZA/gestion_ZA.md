title: Mettre en place une gestion de zone d'activités

La zone d’activités d’aujourd’hui est un lieu de vie économique certes mais aussi celui de la rencontre entre plusieurs communautés d’acteurs : entreprises, fournisseurs, clients, salariés, dirigeants… La ZAE se doit de proposer un environnement, des équipements et une gestion favorable de la vitalité de la ZAE, qui doit être structurée, réfléchie en fonction des besoins des différents « usagers » de la ZAE dans une logique de mise en synergie. La gestion, au-delà des aspects matériels et infrastructures, intègre l’animation, « donner de la vie » et sa coordination.

Les zones d’activités disposant d’une fonction de gestion sont plus robustes, résilientes et souvent mieux vécues par l’ensemble des usagers et riverains. Le terme _gestion_ est en effet un terme large, qui peut recouvrir de nombreux champs d’actions :

* La gouvernance, voire la démocratie de la ZAE pour
    * Définir les orientations pour l’accueil des entreprises, contribuer à la [stratégie économique territoriale](../strategie_territoriale/index.md),
    * Evaluer les besoins en matière de services et les prioriser,
    * Elaborer et faire évoluer les règles de vie commune
    * Faire vivre une démarche qualité ou une démarche environnementale, qu’elle soit portée ou pas par un [label](./labels_ZA)
*  [L'animation](./gestionnaire_animateur)
    * Informer les entreprises,
    * Créer du lien entre les différents acteurs,
    * Initier les synergies entre entreprises,
* La représentation
    * Faire l’interface entre les entreprises occupantes, propriétaires et les acteurs publics ou les riverains,
    * Accueillir les nouvelles entreprises et les accompagner dans leur implantation,
    * Assurer la communication de la zone et sa promotion sur l’ensemble des supports,
* La modernisation et l’amélioration de la ZAE
    * Proposer des évolutions en fonction des opportunités foncières sur le plan de zonage, l’aménagement paysager, l’insertion urbaine...
    * Mener des études et analyses pour identifier les opportunités, la concurrence, anticiper les aléas de marché et l’évolution de la demande,
* La réalisation opérationnelle
    * Assurer la maîtrise d’ouvrage des travaux validés dans le cadre de la gouvernance existante.

La zone d’activités d’aujourd’hui est un lieu de vie économique certes mais aussi celui de la rencontre entre plusieurs communautés d’acteurs : entreprises, fournisseurs, clients, salariés, dirigeants… La ZAE se doit de proposer un environnement, des équipements et une gestion favorable de la vitalité de la ZAE, qui doit être structurée, réfléchie en fonction des besoins des différents « usagers » de la ZAE dans une logique de mise en synergie. La gestion, au-delà des aspects matériels et infrastructures, intègre l’animation, « donner de la vie » et sa coordination.

## Mise en oeuvre
<div alt="parole d'élu >">

!!! quote "parole d'élu"
    Même si ce n'est pas un dû, je considère qu'affecter une partie du temps d'un agent de mes équipes à la gestion de cette zone d'activité est un bon investissement. Elle fonctionne mieux et les entreprises y sont bien.
</div> Il y a de nombreuses modalités pour assurer la gestion d'une zone d'activités économiques. Certains gestionnaires assurent la gestion de plusieurs zones d'activités, travaillent en équipes, certains sont employés par une collectivité locale, d'autres n'assument qu'une petite partie des rôles possibles d'un  gestionnaire de zone.

Il y a une convergence à trouver entre les objectifs des parties prenantes et les moyens qu'ils peuvent y consacrer. En fonction de cet équilibre, il sera possible d'envisager de faire reposer la fonction :

* sur un organisme institutionnel (une intercommunalité, une commune, une chambre consulaire…dans la majeure partie des cas)
* sur une initiative privée en provenance des entreprises usagers de la zone d’activités. Dans ce dernier cas, la création d’une association permet de mieux structurer le processus administratif qui peut être :
    * une mise à disposition d’un salarié d’une entreprise (au travers d’un mécénat de compétences[^1] par exemple)
    * le recrutement direct d’un salarié
    * le recours à une prestation externe de type consultant
    * l’adhésion à un groupement d’employeur avec un salarié à temps partagé

Il est d'ailleurs envisageable, sous réserve d'une coordination adéquate, de répartir les missions d'animation en utilisant au mieux ces différentes solutions.

## Recommandations
Il suffit de voir la liste des actions possible pour constater la complexité de la tâche. Pour mener à bien ses missions, le gestionnaire doit pouvoir s'appuyer sur un mandat clair qui comporte à la fois une définition de ses objectifs et moyens et une organisation de la gouvernance.

Le maintien des objectifs et des principes fixés initialement ne peut être assuré que si tous les intervenants ont la même volonté de mener à bien le projet. Ceci suggère la mise en œuvre d’un partenariat actif et permanent pour assurer la cohérence, la lisibilité et la pérennité du projet. Il est important d’identifier tous les acteurs, leurs rôles, leurs liens et leur niveau d’implication dans la zone d’activités.

L’implication des divers intervenants est à rechercher en amont du projet, dans la définition des objectifs, car il est plus difficile de les solliciter lorsque le projet est déjà lancé. Dans le cadre de l’élaboration de la stratégie de gestion et de la définition des actions, des acteurs moteurs ou dirigeants doivent émerger et seront à même de prendre position et marquer leur engagement dans le projet. Certaines structures souhaiteront s’impliquer davantage et pourront donc constituer les membres d’un _comité de pilotage_ qui prendra les décisions. Les autres partenaires apporteront un soutien ponctuel dans la démarche et participeront aux différents groupes de travail pouvant être organisés.

Un _comité technique_ comprenant les partenaires du parc et les acteurs du territoire, est à envisager car il permet de mobiliser les nombreuses compétences multi domaines que les partenaires pourront apporter. La composition d'un tel comité dépendra des pratiques en vigueur, des objectifs assignés, des modalités de pilotage. Elle doit pouvoir évoluer dans le temps en fonction de ces élément et selon les étapes du projet.

Ces comités sont aussi dépendant des équilibre en place et des participations financières. Ils peuvent être composé de :

* élus et représentants des services de la collectivité concernée (aménagement, développement économique, environnement…) et des services techniques,
* représentants d’entreprises de la zone d’activités;
* partenaires locaux pertinents;
* représentant des riverains de la ZAE;
* …

## Outils juridiques et financiers mobilisables
Les collectivités et/ou les acteurs de la zone devront choisir l’outil juridique pour la gestion de la zone

### Gestion de la zone sous la forme d’un partenariat contractuel
Il est conclu par la signature entre les acteurs d’une « charte » de la gestion de la zone définissant son fonctionnement et les participations matérielles et financières de chacun. A ce titre, certaines entreprises pourront utiliser le mécénat de compétences.

Il faut **bien encadrer la rédaction du contrat** pour éviter les désaccords entre les acteurs sur le financement et notamment en cas de sortie d’une entreprise pour quelle que raison que ce soit.

### Gestion de la zone par une structure juridique ad hoc
Différentes formes juridiques peuvent être adoptées pour cette structure. Les plus simples et adaptées sont sans doute l’association et le GIE.

**L’association**[^asso]

Elle permet de s’organiser librement selon les choix de ses membres fondateurs lors de sa constitution par le contrat d’association qui prend la forme de statuts auxquels les nouveaux membres adhérent également. Son objet peut être large sous réserve de ne pas aboutir au partage des bénéfices entre les membres.

<table>
<thead><tr><th width="49%">Avantages</th><th width="49%">Inconvénients</th></tr></thead>
<tbody>
<tr>
  <td>Collaboration entre partenaires privés et publics possible</td>
  <td rowspan="5">La structure peut être soumise (totalement ou partiellement) aux impôts si elle exerce une ou des activité(s) lucrative(s)</td></tr>
<tr><td>Grande liberté statutaire</td></tr>
<tr><td>Large choix dans la gouvernance</td></tr>
<tr><td>Régime de droit privé du personnel</td></tr>
<tr><td>Comptabilité de droit privé</td></tr>
</tbody>
</table>

Le **groupement d’intérêt économique (GIE)**[^gie]

Il s’agit de constituer entre les membres, pour une durée déterminée, un groupement « en vue de mettre en œuvre tous les moyens propres à faciliter ou à développer l’activité économique de ses membres, à améliorer ou à accroître les résultats de cette activité ». Cette forme juridique est plus encadrée dans sa constitution et son fonctionnement. Notamment, le GIE doit obligatoirement mettre en place un contrôle de gestion exercé par une ou des personnes physiques.

Autre particularité : Les membres du GIE sont tenus responsables indéfiniment et solidairement au paiement des dettes du groupement à l’égard des tiers. Entre eux, le contrat constitutif peut prévoir la contribution de chaque membre au paiement des dettes.

<table>
<thead><tr><th width="49%">Avantages</th><th width="49%">Inconvénients</th></tr></thead>
<tbody>
<tr>
  <td>Grande souplesse dans la rédaction des statuts</td>
  <td rowspan="2">Responsabilité indéfinie et solidaire des membres par rapport aux dettes du groupement (au-delà de la responsabilité commune à tous les dirigeants de structures pour faute de gestion)</td></tr>
<tr><td>Structure adaptée à la mise en commun de moyens et au lancement d’actions communes</td></tr>
<tr>
  <td>Régime de droit privé du personnel</td>
  <td rowspan="2">Mise en œuvre d’un contrôle de gestion</td>
</tr>
<tr><td>Comptabilité de droit privé</td></tr>
</tbody>
</table>

[^1]: Un mécénat de compétences consiste pour une entreprise à mettre à disposition sans contrepartie financière des collaborateurs sur leur temps de travail pour réaliser des actions d’intérêt général mobilisant ou non leurs compétences.
[^asso]: régie par les dispositions de la loi du 1er juillet 1901 et de son décret d’application du 16 août 1901.
[^gie]: régi par les articles L.251-1 à L.251-23 et R.251-1 à R.251-3 du Code de commerce.
