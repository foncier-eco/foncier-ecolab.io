title: Animer une zone d'activités

La fonction d’animation est dissociable de celle de la gestion en cela que cette dernière intègre des aspects plus larges que la seule animation qui est une démarche permettant de

* Informer les entreprises sur la vie de la zone d’activités mais aussi sur les actualités pouvant les intéresser dans leur développement grâce à des supports de communication et des réunions
* Créer du lien entre les différents acteurs de la zone d’activités en organisant des évènements favorisant ces rencontres dans un cadre convivial
* Organiser des rencontres thématiques entre les entreprises

## Mise en oeuvre
Qui dit animation dit animateur, il serait d’ailleurs plus adapté de parler d’animateurs au pluriel. En effet, l’animation repose avant tout sur la création d’une communauté d’acteurs, plus ou moins structurée sur le plan juridique (par exemple au travers d’une association d’entreprises).

Cette vision implique à la fois une coordination et une attribution des missions qui doit se faire en concertation avec la [gestion de la zone](./gestion_ZA) et peut se pratiquer suivant les mêmes modalités.

## Recommandations
On distinguera donc d’une part l’animation qui est avant tout la capacité à concevoir et à générer du « contenu » de la coordination qui elle afin d’être efficace doit reposer sur un ressource humaine dont une partie du temps sera affectée à cette fonction.


Par ailleurs, la mise en place d’une animation / coordination ne se fera pas dans les mêmes conditions en fonction de :

* la vocation de la zone d’activités (industrielle, artisanale, logistique, commerciale, tertiaire…)
* de son niveau de spécialisation (degré de concentration d’entreprises partageant un même secteur d’activités, ex. aéronautique, agroalimentaire, numérique…)
* du nombre d’entreprises implantées
* de la taille des entreprises
* de son ancienneté,
* etc.

## Quelques exemples

### Journée portes ouvertes sur le principe de la « fête des voisins »
Pour une entreprise, « Ouvrir ses portes » ne permet ainsi pas uniquement de répondre à une attente des visiteurs, il s’agit également d’une stratégie de communication à part entière auprès des consommateurs, des prescripteurs, de partenaires institutionnels ou tout simplement auprès des habitants du territoire et des riverains de l’établissement.  C’est aussi un « acte de management » à destination des collaborateurs en leur permettant de valoriser leur travail vis-à-vis de l’extérieur.

Organisée à l’échelle d’une ZAE en impliquant le plus grand nombre d’entreprises qui y sont implantées permet aussi de participer à l’animation de la destination économique Dinan Agglomération et favoriser la transversalité entre des populations de collaborateurs qui ne se croisent souvent jamais. Cela permet de créer du lien humain dans les ZAE qui sont souvent vécus comme des espaces « manquant de vie ».

### Elaboration d’un « pack de bienvenue » pour les entreprises qui envisagent de s’implanter sur le territoire
Pour favoriser l’arrivée de nouvelles entreprises sur le territoire, il pourrait être élaboré un bouquet d’avantages et de services « VIP » à l’instar de ce qui se fait dans d’autres territoires pour les créateurs ou repreneurs d'activités professionnelles et aux salariés. Ce « pack » pourrait tout à la fois être constitué de soutiens à visée professionnelle qu’à destination de l’intégration personnelle du dirigeant et de ses premiers collaborateurs sur le territoire. Citons quelques propositions :

* Prestations avantageuses grâce à des partenariats avec des entreprises partenaires de la démarche du territoire (par exemple, conseils gratuits auprès des notaires, avocats, expert comptables, remise tarifaires sur des prestations informatiques et logiciels, remise tarifaire sur des assurances, remise forfaitaire sur les publicités, mailing, etc.)
* Appui à l’installation familiale : facilitation des démarches administratives, recherche de logement avec les agents immobiliers, découverte des équipements et services à la population…
* Accueil culturel et touristique : réduction ou gratuités pour des visites et produits touristiques et des évènements culturels

### Organisation d’animations du type réunions ou petits déjeuners
Le choix des thèmes de rencontres est à faire de façon judicieuse. Pour cela l’identification des thèmes potentiels d’animation doit se faire en questionnant les entreprises, certes du point de vue de leurs responsables et, cela est souhaitable, aussi du point de vue des collaborateurs. Ce questionnement peut être réaliser au travers d’un questionnaire (par ex. administré en ligne grâce à une plateforme gratuite d’enquête) ou lors de rendez-vous individuels entre le coordinateur de l’animation et des usagers de la ZAE. 
