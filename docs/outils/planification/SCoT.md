title: SCoT
delai: 40
duree: 200
pilote: EPCI
summary: A l’échelle d’un large bassin d’emploi ou d’une aire urbaine, le projet d’aménagement du SCoT est l’outil privilégié de déclinaison intercommunale de la stratégie économique.

!!! info "ébauche"
    cette fiche est une ébauche. N'hésitez pas à apporter votre expérience et vos compétences pour son amélioration. (cf. [contribuer](../../a_propos/contribuer.md))

Outil de conception et de mise en œuvre d’une planification stratégique intercommunale, à l’échelle d’un large bassin d’emploi ou d’une aire urbaine, dans le cadre d’un projet d’aménagement et de développement durables. Il est l’outil privilégié de déclinaison intercommunale de la stratégie régionale définie par le [SRADDET](./SRADDET) et le [SRDEII](./SRDEII).

## Pilotage et partenariats
le SCoT est élaboré et mis en œuvre par l’[EPCI](./EPCI) du territoire concerné ou par un syndicat mixte réunissant les EPCI concernés. Il associe les partenaires essentiels de l’aménagement du territoire. En matière d’espaces d’activités, il est particulièrement recommandé que soit associés …

## Mise en oeuvre
La durée d’élaboration d’un SCoT varie généralement entre 3 et 5 ans. Un projet de territoire bien avancé et des dynamiques de collaboration sont des atouts pour un aboutissement rapide.

##Recommandations
Le SCoT permet d’établir à une échelle intercommunale adaptée un projet d’aménagement et de développement durable accompagné pour sa mise en œuvre de prescriptions. Il doit également définir la planification de l’utilisation des espaces (notamment ceux dédiés aux ZAE)[^cu].

Le SCoT est naturellement plus adapté aux cas décrit dans les scénarios xx et xx. Toutefois il trouve également son intérêt dans le scénario xx, car …

Intermédiaire entre le SRADDET et le [PLUi](../planification/PLUi.md), il peut tirer bénéfice des [contrats de territoires](../projets_contrats/contrats_de_territoires.md) ou d’une démarche d’[Agenda 21](../projets_contrats/agenda_21.md) pour optimiser l’usage du foncier.

Le bilan et l’évaluation du SCoT, obligatoires au plus tard 6 ans après son approbation sont une opportunité pour évaluer le projet et sa mise en œuvre, notamment en terme d’activités économiques

## Références et guides

[^cu]: articles L131-1 à L131-3 et L141-1 à L144-1 du code de l’urbanisme. A noter en particulier le [volet équipement commercial et artisanal du DOO](./SCoT/DOO_volet_equipement_commercial_artisanal.md) ainsi que les articles L143-32 à L143-39 (modification de droit commun et modification simplifiée)
