title: Observatoire du foncier économique

Dans le but de disposer d’une vision claire, exhaustive et actualisée du marché du foncier économique du territoire, et ainsi répondre rapidement et efficacement aux demandes des entreprises, les territoires devraient se doter d’un observatoire du foncier économique destiné à orienter et évaluer la politique foncière de la collectivité et prévenir les mutations foncières à venir.

## Pilotage et Mise en oeuvre

Les objectifs de l'observatoire sont à définir finement en relation avec la [stratégie économique du territoire][strategie_eco_territoriale]. Dans l'idéal, l'observatoire comprend plusieurs volets :

* Un état des lieux quantitatif et qualitatif
    * La situation du foncier économique et de l’immobilier d’entreprises disponibles
    * La consommation d’espace par les activités économiques et son historique
    * La connaissance des activités économiques implantées sur le territoire
    * L’observation des effets externes liés à la présence de ZAE
    * L'identification des gisements fonciers du territoire, notamment en centre-ville et centre-bourg, ainsi que les friches existantes ou potentielles

* Des éléments d'analyse stratégique
    * Une compréhension des parcours résidentiels des entreprises sur ce territoire
    * Une vision prospective des besoins économiques à l’échelle du territoire pour l’ensemble des collectivités concernées et de leurs partenaires

* Des recommandations et pistes de travail pour
    * Favoriser une consommation foncière raisonnée et en adéquation avec la demande des entreprises
    * Proposer des orientations privilégiées des espaces à vocation économiques adaptées aux atouts du territoires  
    * Fournir une vision claire de l'organisation inter-territoriale en matière d'activités économiques, par exemple via une typologie d’entreprises et des besoins associés
    * Définir les emprises foncières nécessaires[^1] et une segmentation de l’offre foncière en différents « produits »

## Recommandations
La mise en oeuvre de dispositifs d'analyse et d'observation est à terme une importante source d'économie pour les territoires. Pour autant les investissements nécessaires sont immédiats et peuvent être perçus comme lourds. Aussi, il est en général pertinent de s'entourer des partenaires qui travaillent déjà sur ces questions localement et de mutualiser autant que possible les efforts avec les territoires voisins. Ces partenariats seront également profitables au stade des recommandations et de la construction stratégique.

Certains sujets pourtant importants pour la vie des ZAE sont malheureusement, fréquemment _mis de côté_. Citons par exemple :

* Le statut des espaces économiques au regard des documents d’urbanisme en vigueur
* La localisation des espaces économiques dans une logique d’équilibre de l’aménagement du territoire en prenant en compte tout particulièrement les questions de mobilité[^2] (ex. trajet domicile-travail) et de logement
* Les services nécessaires aux entreprises et aux salariés
* L’aire de concurrence du territoire défini comme l’ensemble des espaces économiques avec lesquelles le territoire d’étude connaît des transferts d’entreprises (entrants et sortants).
* Une réflexion sur une politique tarifaire différenciée qui appuyera la stratégie du territoire

[^1]: le besoin en foncier varie selon que l’entreprise relève du tertiaire ou de l’industrie
[^2]: les questions des trajets domiciles-travail, les facilités en terme d'intermodalité (transport collectif, aire de covoiturage, proximité aux axes structurants…)

!!! doc "Aller plus loin"
    * [Observer et connaître les zones d’activités économiques](../../references/syntheses/observer_connaitre_ZAE-cerema.md) - synthèse du Cerema
    * [Cartofriches][cartofriches.md], un outil pour capitaliser et mutualiser la connaissance des friches

[strategie_eco_territoriale]: ../strategie_territoriale/index.md
