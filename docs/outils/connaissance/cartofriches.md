title: Cartofriches

[Cartofriches][cartofriches] est un outil développé par le CEREMA pour aider au recensement des friches (industrielles, commerciales, d’habitat, tertiaires, etc.), ouvert au grand public via un portail de visualisation sur Internet.

En proposant une base nationale de sites potentiellement en friches, mais surtout en s’appuyant sur les connaissances locales (observatoires locaux, expertises terrain…), l’objectif est de promouvoir et faciliter la réutilisation des friches par les collectivités locales et les porteurs de projets.

L'outil existe dans une version de test, notamment basée sur quelques observatoires locaux. Il est donc possible d'expérimenter l'outil et, bien entendu, de contribuer à l'enrichir.

!!! doc "Aller plus loin"
    * [Les enjeux d'un inventaire pour la revitalisation des friches](https://artificialisation.biodiversitetousvivants.fr/cartofriches/enjeux-revitalisation-friches) - Portail de l'artificialisation des sols

[cartofriches]: https://cartofriches.cerema.fr/cartofriches/
