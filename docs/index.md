title: Foncier Eco

Ce site sur le **Foncier** et l'**Economie** est une expérience menée dans le cadre de la réalisation de l'[étude pour l'optimisation du foncier d'activité][etude_optimisation_foncier_activite]. Partant du constat que la description des outils et méthodes foncières s'accomodait mal d'une présentation linéaire et nécessitait des mises à jour régulières, nous avons imaginé utiliser le principe du [wiki][wiki] pour les présenter. Dans cette logique, le site est largement ouvert aux [contributions](./a_propos/contribuer.md).

<div markdown="1" class="three cols">
!!! gear "[outils](./outils/)"
    La raison d'être principale de ce site : des outils et méthodes pour améliorer l'efficacité du foncier économique

!!! doc "[références](./references/)"
    De la documentation, des guides, des retours d'expérience pour inspirer les projets

!!! question "[à propos](./a_propos/)"
    Ce que vous devez savoir sur ce projet et comment contribuer ou faire des remarques

</div>

Une entrée par _acteur_ est également prévue, mais n'a pas encore été mise en place.

[etude_optimisation_foncier_activite]: ./references/etudes/etude_optimisation_foncier_activite/index.md
[wiki]: https://fr.wikipedia.org/wiki/Wiki
